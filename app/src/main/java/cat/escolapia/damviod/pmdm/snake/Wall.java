package cat.escolapia.damviod.pmdm.snake;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alejandro.paris on 07/12/2016.
 */
public class Wall {
    public List<WallPart> paret = new ArrayList<WallPart>();
    public int x, y;

    public Wall(int x, int y) {
        this.x = x;
        this.y = y;
        paret.add(new WallPart(x, y));
        paret.add(new WallPart(x+1, y));
        paret.add(new WallPart(x+2, y));
        //this.type = type;
    }
}
