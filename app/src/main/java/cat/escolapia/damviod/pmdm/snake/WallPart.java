package cat.escolapia.damviod.pmdm.snake;

/**
 * Created by alejandro.paris on 07/12/2016.
 */
public class WallPart {
    public int x, y;

    public WallPart(int x, int y) {
        this.x = x;
        this.y = y;
    }
}
