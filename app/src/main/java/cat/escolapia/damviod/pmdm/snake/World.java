package cat.escolapia.damviod.pmdm.snake;

import java.util.Random;

public class World {
    static final int WORLD_WIDTH = 10;
    static final int WORLD_HEIGHT = 13;
    static final int SCORE_INCREMENT = 10;
    static final float TICK_INITIAL = 0.5f;
    static final float TICK_DECREMENT = 0.05f;

    public Snake snake;
    public Diamond diamond;
    public Diamond diamond1;
    public Wall wall;
    public boolean gameOver = false;;
    public int score = 0;

    boolean fields[][] = new boolean[WORLD_WIDTH][WORLD_HEIGHT];
    Random random = new Random();
    float tickTime = 0;
    float tick = TICK_INITIAL;

    public World() {
        snake = new Snake();
        wall = placeWall();
       diamond = placeDiamond();
       diamond1 = placeDiamond();
    }

    private Diamond placeDiamond() {

        for (int x = 0; x < WORLD_WIDTH; x++) {
            for (int y = 0; y < WORLD_HEIGHT; y++) {
                fields[x][y] = false;
            }
        }

        int len = snake.parts.size();
        for (int i = 0; i < len; i++) {
            SnakePart part = snake.parts.get(i);
            fields[part.x][part.y] = true;
        }

        int diamondX = random.nextInt(WORLD_WIDTH);
        int diamondY = random.nextInt(WORLD_HEIGHT);
        while (true) {
            if (fields[diamondX][diamondY] == false) break;
            diamondX += 1;
            if (diamondX >= WORLD_WIDTH) {
                diamondX = 0;
                diamondY += 1;
                if (diamondY >= WORLD_HEIGHT) {
                    diamondY = 0;
                }
            }
        }
       return new Diamond(diamondX, diamondY, Diamond.TYPE_1);
    }

    private Wall placeWall() {

        for (int x = 0; x < WORLD_WIDTH; x++) {
            for (int y = 0; y < WORLD_HEIGHT; y++) {
                fields[x][y] = false;
            }
        }

        int len = snake.parts.size();
        for (int i = 0; i < len; i++) {
            SnakePart part = snake.parts.get(i);
            fields[part.x][part.y] = true;
        }

        int wallX = random.nextInt(WORLD_WIDTH);
        int wallY = random.nextInt(WORLD_HEIGHT);
        while (true) {
            if (fields[wallX][wallY] == false) break;
            wallX += 1;
            if (wallX >= WORLD_WIDTH) {
                wallX = 0;
                wallY += 1;
                if (wallY >= WORLD_HEIGHT) {
                    wallY = 0;
                }
            }
        }
        return new Wall(wallX,wallX);
    }

    public void update(float deltaTime) {
        if (gameOver) return;
        tickTime += deltaTime;

        while (tickTime > tick) {
            tickTime -= tick;
            snake.advance();
            if (snake.checkXoca()) {
                gameOver = true;
                return;
            }
            if (snake.checkDiamant(diamond)) {
                score++;
                tick -= TICK_DECREMENT/2;
                diamond = placeDiamond();
                snake.allarga();
                return;
            }
            if (snake.checkDiamant(diamond1)) {
                score++;
                tick -= TICK_DECREMENT/2;
                diamond1 = placeDiamond();
                snake.allarga();
                return;
            }
            if (snake.checkWall(wall)) {
                gameOver = true;
                return;
            }
            SnakePart head = snake.parts.get(0);
            //Comprovació x i y dels head igual a diamant.
            // Si sí, score = score + INCREMENTSCORE, allarga serp i cridar a placeDiamond de nou... adicionalment
            // es pot baixar el tick per augmentar la dificultat
        }
    }
}
